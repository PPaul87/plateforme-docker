<?php
/**
 * Created by PhpStorm.
 * User: lpdw
 * Date: 11/10/18
 * Time: 09:21
 */

declare(strict_types=1);
namespace App\Repository;
use App\Entity\Article;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use DateTime;
class ArticleRepository
{
    private $articleList;
    public function __construct()
    {
        $this->articleList = new ArrayCollection();
        $article = new Article();
        $article->setId(1)
            ->setText("Premier Message")
            ->setCreatedAt(new DateTime());
        $this->articleList->add($article);
    }
    public function findAll(): Collection
    {
        return $this->articleList;
    }
}
